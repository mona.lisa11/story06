from django.conf.urls import url
from .views import landingpage, myStory8

urlpatterns = [
	url(r'^$', landingpage , name='landingpage'),
	url(r'^myStory8', myStory8, name='myStory8'),
	
	#untuk profile atau yang lain gini templatenya
	#url(r'^profile', profile , name='profile')

]
