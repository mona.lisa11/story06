from django.shortcuts import render

# Create your views here.
def landingpage(request):
	return render(request, "landingpage.html")

def myStory8(request):
	return render(request, "myStory8.html")
